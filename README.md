# LabVIEW interface

This is a basic Labview example for reading the measurements of bota systems serial devices. Compatible with Bota Systems AG serial sensors.

# Bugs & Feature Requests #

Please report bugs and request features using the [Issue Tracker](https://gitlab.com/botasys/labview_interface/-/issues).
